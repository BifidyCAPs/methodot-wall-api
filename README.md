# Build

```
docker build -t wishingwall_api:latest .
```

# Pull

```
docker tag wishingwall_api:latest registry.cn-hongkong.aliyuncs.com/methodot/wishingwall_api:latest
```

# Run

```
docker run --name wishingwall_api  wishingwall_api\
```

