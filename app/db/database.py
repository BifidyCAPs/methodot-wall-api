from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from os import environ
from fastapi import Depends
from sqlalchemy import Column, Integer, DateTime, String, text
## ENV

MYSQL_HOST = environ.get('MYSQL_HOST', "")
MYSQL_PORT = environ.get('MYSQL_PORT', "3306")
MYSQL_USER = environ.get('MYSQL_USER', "root")
MYSQL_PWD = environ.get('MYSQL_PWD', "")
MYSQL_DB_NAME = environ.get('MYSQL_DB_NAME', "wall")

## Connection

CommentBase = declarative_base()

def create_session() -> Session:
    engine = create_engine(f"mysql+pymysql://{MYSQL_USER}:{MYSQL_PWD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DB_NAME}")
    return sessionmaker(bind=engine, autocommit=False, autoflush=False)
    
## Depend

LocalSession = create_session()

def get_db():
    db = LocalSession()
    try:
        yield db
    finally:
        db.close()

db_depends = Depends(get_db)

class Comment(CommentBase):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(String(40), nullable=False)
    username = Column(String(255), nullable=False)
    content = Column(String(1024), nullable=False)
    create_time = Column(DateTime, server_default=text('CURRENT_TIMESTAMP'), nullable=False)
    # new version
    # status = Column(String(40), nullable=True, server_default=text("'pendding'"))
    # ===