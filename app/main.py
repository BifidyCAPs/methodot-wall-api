from fastapi import FastAPI 
from .view import user_api, comment_api

app = FastAPI()
app.include_router(user_api, prefix="/api/user")
app.include_router(comment_api, prefix="/api/comment")

if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app=app, host='0.0.0.0', port=8000)
