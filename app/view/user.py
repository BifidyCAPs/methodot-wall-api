from pydantic import BaseModel
from fastapi import APIRouter, Depends, HTTPException
import requests
from os import environ

api = APIRouter(tags=["user"])

USER_HOST = environ.get("USER_HOST", "")  + ":8080"
USER_ADMIN_NAME = environ.get("USER_ADMIN_NAME", "bob")
USER_ADMIN_PWD = environ.get("USER_ADMIN_PWD", "")


class UserIn(BaseModel):
    username: str
    password: str

class UserOut(BaseModel):
    username: str
    user_id: str
    token: str = None


@api.post("/login", response_model=UserOut)
def login(user: UserIn):
    admin_token = get_admin_token()

    if not admin_token:
        raise HTTPException(status_code=501, detail="User Service is not available")
    
    resp = requests.post(url=f"http://{USER_HOST}/v1/users", json=user.dict(), headers={"Authorization": f"Bearer {admin_token}"})

    token = get_user_token(user.username, user.password)
    if not token:
        raise HTTPException(status_code=401, detail="Invalid username or password")

    user_id = get_user_id(user.username, token)
    if not user_id:
        raise HTTPException(status_code=401, detail="Invalid user info")

    return UserOut(username=user.username, user_id=user_id, token=token)


def get_admin_token() -> str:
    return get_user_token(USER_ADMIN_NAME, USER_ADMIN_PWD)

def get_user_token(username:str , password:str) -> str:
    d = dict(login=username, password=password)
    resp = requests.post(url=f"http://{USER_HOST}/login", data=d)
    if resp.status_code == 200:
        return resp.json().get("accessToken")
    else:
        return None

def get_user_id(login:str , token:str) -> str:
    resp = requests.get(url=f"http://{USER_HOST}/v1/users/{login}", headers={"Authorization": f"Bearer {token}"})
    if resp.status_code == 200:
        return str(resp.json().get("id"))
    else:
        return None
