from app.db import  db_depends, Comment as CommentBase
from sqlalchemy.orm import Session
from sqlalchemy import or_
from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime
from fastapi import APIRouter, HTTPException, Header
from .user import get_user_id




class Comment(BaseModel):
    id: int
    username: str
    user_id: str
    content: str
    create_time: datetime
    # new version
    # status: str
    # ===


    class Config:
        orm_mode = True

class CommentIn(BaseModel):
    username: str
    user_id: str
    content: str

api = APIRouter(tags=["comment"])

@api.get("/", response_model=List[Comment])
def get_comments(db: Session = db_depends, skip: int = 0, limit: int = 100, username: Optional[str] = None, access_token: Optional[str] = Header(None)):
    # old version
    comments = db.query(CommentBase).order_by(CommentBase.create_time.desc()).offset(skip).limit(limit).all()
    # new version
    # comments = db.query(CommentBase)
    # if username and access_token:
    #     user_id = get_user_id(login=username, token=access_token)
    #     comments = comments.filter(or_(CommentBase.user_id == user_id, CommentBase.status == "ok"))
    # else:
    #     comments = comments.filter(CommentBase.status == "ok")
    # comments = comments.order_by(CommentBase.create_time.desc()).offset(skip).limit(limit).all()
    # ===
    return comments

@api.post("/", response_model=Comment)
def post_comment(comment: CommentIn, db: Session = db_depends , access_token: str = Header(None)):
    if comment.user_id != get_user_id(login=comment.username, token=access_token):
        return HTTPException(status_code=401, detail="Invalid user")
    db_comment = CommentBase(**comment.dict())
    db.add(db_comment)
    db.commit()
    db.refresh(db_comment)
    return db_comment

@api.delete("/{comment_id}", response_model=Comment)
def delete_comment(comment_id: int, db: Session = db_depends, access_token: str = Header(None)):
    db_comment = db.query(CommentBase).filter(CommentBase.id == comment_id).first()
    if db_comment.user_id != get_user_id(login=db_comment.username, token=access_token):
        return HTTPException(status_code=401, detail="Invalid user")
    db.delete(db_comment)
    db.commit()
    return db_comment
