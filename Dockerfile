FROM python:3.8

WORKDIR /workspace
COPY ./requirements.txt /workspace/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /workspace/requirements.txt
COPY ./app /workspace/app
COPY ./log_config.yaml /workspace/log_config.yaml
EXPOSE 80
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80", "--log-config", "./log_config.yaml"]
